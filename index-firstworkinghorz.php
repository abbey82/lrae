<!doctype html>
<html class="" lang="en">
    <head>

    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.min.css"> -->

<style>

    .cellTest {
        display:inline-block;
        width: 100vw;
        height: 100vh;
        position: relative;
        z-index: 1;
    }







*,
*::after,
*::before {
    box-sizing: border-box;
}

:root {
    font-size: 12px;
}

body {
    margin: 0;
    --color-text: #000100;
    --color-bg: #e2d9c7;
    --color-link: #395bb5;
    --color-link-hover: #000100;
    --color-gallery-title: #2c2724;
    --color-gallery-number: #d9d0be;
    --color-gallery-link: #fff;
    --color-gallery-link-bg: #2858d7;
    --color-gallery-link-hover: #fff;
    --color-gallery-link-bg-hover: #d4b77d;
    color: var(--color-text);
    background-color: var(--color-bg);
    --cursor-stroke: none;
    --cursor-fill: #c5681c;
    --cursor-stroke-width: 1px;
    font-family: halyard-display, sans-serif;
    font-weight: 300;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

/* Page Loader */
.js .loading::before,
.js .loading::after {
    content: '';
    position: fixed;
    z-index: 1000;
}

.js .loading::before {
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: var(--color-bg);
}

.js .loading::after {
    top: 50%;
    left: 50%;
    width: 60px;
    height: 60px;
    margin: -30px 0 0 -30px;
    border-radius: 50%;
    opacity: 0.4;
    background: var(--color-link);
    animation: loaderAnim 0.7s linear infinite alternate forwards;

}

@keyframes loaderAnim {
    to {
        opacity: 1;
        transform: scale3d(0.5,0.5,1);
    }
}

a {
    text-decoration: none;
    color: var(--color-link);
    outline: none;
}

a:hover,
a:focus {
    color: var(--color-link-hover);
    outline: none;
}

.frame {
    padding: 3rem 5vw;
    text-align: center;
    position: relative;
    z-index: 1000;
    text-transform: uppercase;
}

.frame__title {
    font-size: 1rem;
    margin: 0 0 1rem;
    font-weight: 300;
}

.frame__links {
    display: inline;
}

.frame__links a:not(:last-child) {
    margin-right: 1rem;
}

.frame__demo {
    margin: 0 1rem;
}

.frame__demo--current,
.frame__demo--current:hover {
    color: var(--color-text);
    cursor: default;
}

.content {
    display: flex;
    flex-direction: column;
    height: calc(100vh - 13rem);
    position: relative;
    justify-content: flex-start;
}

.cursor {
    display: none;
}

@media screen and (min-width: 53em) {
    .frame {
        position: fixed;
        text-align: left;
        z-index: 100;
        top: 0;
        left: 0;
        display: grid;
        align-content: space-between;
        width: 100%;
        max-width: none;
        padding: 1.75rem;
        pointer-events: none;
        grid-template-columns: auto 1fr 1fr auto;
        grid-template-rows: auto;
        grid-template-areas: 'title demos demos links';
    }
    .frame__title {
        margin: 0 4rem 0 0;
        grid-area: title;
    }
    .frame__info {
        grid-area: info;
    }
    .frame__demos {
        grid-area: demos;
    }
    .frame__links {
        grid-area: links;
        padding: 0;
        justify-self: end;
    }
    .frame a {
        pointer-events: auto;
    }
    .content {
        height: 100vh;
        justify-content: center;
    }
}

@media (any-pointer:fine) {
    .cursor {
        position: fixed;
        top: 0;
        left: 0;
        display: block;
        pointer-events: none;
    }

    .cursor__inner {
        fill: var(--cursor-fill);
        stroke: var(--cursor-stroke);
        stroke-width: var(--cursor-stroke-width);
        opacity: 0.7;
    }

    .no-js .cursor {
        display: none;
    }

}

/*! locomotive-scroll v4.0.4 | MIT License | https://github.com/locomotivemtl/locomotive-scroll */
html.has-scroll-smooth {
    overflow: hidden; }
  
  html.has-scroll-dragging {
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none; }
  
  .has-scroll-smooth body {
    overflow: hidden; }
  
  .has-scroll-smooth [data-scroll-container] {
    min-height: 100vh; }
  
  [data-scroll-direction="horizontal"] [data-scroll-container] {
    white-space: nowrap;
    height: 100vh;
    display: inline-block;
    white-space: nowrap; }
  
  [data-scroll-direction="horizontal"] [data-scroll-section] {
    display: inline-block;
    vertical-align: top;
    white-space: nowrap;
    height: 100%; }
  
  .c-scrollbar {
    position: absolute;
    right: 0;
    top: 0;
    width: 11px;
    height: 100%;
    transform-origin: center right;
    transition: transform 0.3s, opacity 0.3s;
    opacity: 0; }
    .c-scrollbar:hover {
      transform: scaleX(1.45); }
    .c-scrollbar:hover, .has-scroll-scrolling .c-scrollbar, .has-scroll-dragging .c-scrollbar {
      opacity: 1; }
    [data-scroll-direction="horizontal"] .c-scrollbar {
      width: 100%;
      height: 10px;
      top: auto;
      bottom: 0;
      transform: scaleY(1); }
      [data-scroll-direction="horizontal"] .c-scrollbar:hover {
        transform: scaleY(1.3); }
  
  .c-scrollbar_thumb {
    position: absolute;
    top: 0;
    right: 0;
    background-color: black;
    opacity: 0.5;
    width: 7px;
    border-radius: 10px;
    margin: 2px;
    cursor: -webkit-grab;
    cursor: grab; }
    .has-scroll-dragging .c-scrollbar_thumb {
      cursor: -webkit-grabbing;
      cursor: grabbing; }
    [data-scroll-direction="horizontal"] .c-scrollbar_thumb {
      right: auto;
      bottom: 0; }






.gallery {
    display: flex;
    margin-left: 12vw;
    padding-right: 12vw;
}

.gallery__item {
    margin: 0 3vw;
    display: grid;
    grid-template-areas: '... ...' 
                         '... gallery-image'
                         '... ...' ;
    grid-template-columns: 8rem 21vmax;
    grid-template-rows: 8rem 28vmax 3rem;
}

.gallery__item:nth-child(even) {
    padding-top: 10vh;
}

.gallery__item-img {
    grid-area: gallery-image;
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
    transform-origin: 50% 100%;
}

.gallery__item-imginner {
    background-size: cover;
    background-position: 50% 0;
    width: 100%;
    height: 100%;
}

.gallery__item-caption {
    grid-area: 1 / 1 / 4 / 3;
    display: grid;
    grid-template-areas: 'gallery-number gallery-title' 
                         'gallery-link ...'
                         'gallery-link gallery-tags' ;
    grid-template-columns: 8rem auto;
    grid-template-rows: 8rem auto 3rem;

}

.gallery__item-number {
    grid-area: gallery-number;
    font-size: 6rem;
    font-size: clamp(2.5rem,9vw,6.5rem);
    justify-self: end;
    padding-right: 2rem;
    color: var(--color-gallery-number);
}

.gallery__item-title {
    grid-area: gallery-title;
    margin: 0;
    font-size: 4rem;
    font-size: clamp(2rem,5vw,4rem);
    color: var(--color-gallery-title);
}

.gallery__item-number,
.gallery__item-title,
.gallery__text {
    font-family: moret, serif;
    font-weight: 800;
    font-style: italic;
    align-self: center;
}

.gallery__item-link {
    grid-area: gallery-link;
    align-self: end;
    font-size: 1.5rem;
    background: var(--color-gallery-link-bg);
    color: var(--color-gallery-link);
    text-decoration: underline;
    width: 120px;
    height: 120px;
    border-radius: 50%;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
}

.gallery__item-link:focus,
.gallery__item-link:hover {
    background: var(--color-gallery-link-bg-hover);
    color: var(--color-gallery-link-hover);
    text-decoration: none;
}

.gallery__item-tags {
    grid-area: gallery-tags;
    justify-self: end;
    font-size: 1.5rem;
    display: grid;
    grid-auto-flow: column;
    grid-gap: 1rem;
    cursor: pointer;
}

.gallery__text {
    font-size: 20vw;
    line-height: 0.8;
    margin: 0 10vw 0 14vw;
    text-transform: lowercase;
    color: var(--color-gallery-title);
    color: transparent;
    -webkit-text-stroke: 1px #978c77;
    text-stroke: 1px #978c77;
    -webkit-text-fill-color: transparent;
    text-fill-color: transparent;
}

.gallery__text-inner {
    display: block;
}    
</style>






</head>
<body>



        <main data-scroll-container>
            <div class="content">
                <div class="gallery">
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/1.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll data-scroll-speed="1">Funambulist</h2>
                            <span class="gallery__item-number">01</span>
                            <p class="gallery__item-tags">
                                <span>#house</span>
                                <span>#green</span>
                                <span>#chair</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/2.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Omophagy</h2>
                            <span class="gallery__item-number">02</span>
                            <p class="gallery__item-tags">
                                <span>#love</span>
                                <span>#hug</span>
                                <span>#people</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/3.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Conniption</h2>
                            <span class="gallery__item-number">03</span>
                            <p class="gallery__item-tags">
                                <span>#hike</span>
                                <span>#nature</span>
                                <span>#rain</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/4.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Xenology</h2>
                            <span class="gallery__item-number">04</span>
                            <p class="gallery__item-tags">
                                <span>#free</span>
                                <span>#wood</span>
                                <span>#fire</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <div class="gallery__text"><span class="gallery__text-inner" data-scroll data-scroll-speed="3">Pollex</span><span data-scroll data-scroll-speed="1" class="gallery__text-inner">Mallam</span></div>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/5.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Lycanthropy</h2>
                            <span class="gallery__item-number">05</span>
                            <p class="gallery__item-tags">
                                <span>#cloud</span>
                                <span>#lake</span>
                                <span>#frog</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/6.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Mudlark</h2>
                            <span class="gallery__item-number">06</span>
                            <p class="gallery__item-tags">
                                <span>#tent</span>
                                <span>#flower</span>
                                <span>#love</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/7.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Illywhacker</h2>
                            <span class="gallery__item-number">07</span>
                            <p class="gallery__item-tags">
                                <span>#water</span>
                                <span>#bottle</span>
                                <span>#hand</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/8.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Disenthral</h2>
                            <span class="gallery__item-number">08</span>
                            <p class="gallery__item-tags">
                                <span>#night</span>
                                <span>#stars</span>
                                <span>#moon</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <div class="gallery__text"><span class="gallery__text-inner" data-scroll data-scroll-speed="1">Cacoe</span><span data-scroll data-scroll-speed="3" class="gallery__text-inner">Dupis</span></div>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/9.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Abaya</h2>
                            <span class="gallery__item-number">09</span>
                            <p class="gallery__item-tags">
                                <span>#sun</span>
                                <span>#light</span>
                                <span>#air</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/10.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Hallux</h2>
                            <span class="gallery__item-number">10</span>
                            <p class="gallery__item-tags">
                                <span>#vital</span>
                                <span>#fog</span>
                                <span>#close</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/11.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Lablab</h2>
                            <span class="gallery__item-number">11</span>
                            <p class="gallery__item-tags">
                                <span>#cover</span>
                                <span>#bed</span>
                                <span>#window</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <figure class="gallery__item">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/img/demo1/12.jpg)"></div></div>
                        <figcaption class="gallery__item-caption">
                            <h2 class="gallery__item-title" data-scroll  data-scroll-speed="1">Momisom</h2>
                            <span class="gallery__item-number">12</span>
                            <p class="gallery__item-tags">
                                <span>#sad</span>
                                <span>#mouth</span>
                                <span>#tear</span>
                            </p>
                            <a class="gallery__item-link">explore</a>
                        </figcaption>
                    </figure>
                    <div class="gallery__text"><span class="gallery__text-inner" data-scroll data-scroll-speed="4">Chad</span><span data-scroll data-scroll-speed="1" class="gallery__text-inner">Chiliad</span></div>
                </div>
            </div>
        </main>





</body>

<!-- <script nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
<script nomodule src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CElement.prototype.append%2CNodeList.prototype.forEach%2CCustomEvent%2Csmoothscroll" crossorigin="anonymous"></script> -->



<!-- <script src="<?php echo get_template_directory_uri(); ?>/js/example-main.js"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/js/locomotive-scroll.min.js"></script>

<script type="text/javascript">
(function(){

    // import {preloadImages, preloadFonts} from '../utils';
    // import Cursor from '../cursor';
    // import LocomotiveScroll from 'locomotive-scroll';

    // Initialize Locomotive Scroll (horizontal direction)
    const lscroll = new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true,
        direction: 'horizontal'
    });

    /*
    // Preload images and fonts
    Promise.all([preloadImages('.gallery__item-imginner'), preloadFonts('vxy2fer')]).then(() => {
        // Remove loader (loading class)
        document.body.classList.remove('loading');

        // Initialize custom cursor
        const cursor = new Cursor(document.querySelector('.cursor'));

        // Mouse effects on all links and others
        [...document.querySelectorAll('a')].forEach(link => {
            link.addEventListener('mouseenter', () => cursor.enter());
            link.addEventListener('mouseleave', () => cursor.leave());
        });
    });
    */

})();
</script>

</html>