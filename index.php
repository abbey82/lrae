<!doctype html>
<html class="" lang="en">
    <head>

    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.min.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css"> -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lraeStyle.css">
    <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />

<style>


</style>






</head>
<body>

    <?php 

        include get_template_directory() . '/nav.php';

    ?>



        <main data-scroll-container>
            <div class="content">
                <div class="gallery">
                    
                    
                    
                    

                    <figure class="gallery__item inline-block" style="width:100vw">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                


                                <video
                                    id="my-video-2"
                                    class="video-js"
                                    <?/*controls*/?>
                                    preload="auto"
                                    width="640"
                                    height="264"
                                    autoplay=true
                                    loop=true
                                    muted=true

                                    data-setup="{}"
                                  >
                                    <source src="<?php echo get_template_directory_uri(); ?>/video/2021-01-09_test1.mp4" type="video/mp4" />
                                    <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
                                    <p class="vjs-no-js">
                                      To view this video please enable JavaScript, and consider upgrading to a
                                      web browser that
                                      <a href="https://videojs.com/html5-video-support/" target="_blank"
                                        >supports HTML5 video</a
                                      >
                                    </p>
                                  </video>



                            </div>
                        </div>
                    </figure>
                    
                    
                    
                    
                    
                    <figure class="gallery__item">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)"></div>
                        </div>
                    </figure>


                    <figure class="gallery__item x150">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)"></div></div>
                    </figure>


                    <figure class="gallery__item cropInsert alignCenter">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)"></div></div>
                    </figure>



                    <figure class="gallery__item inline-block">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg" />
                            </div>
                        </div>
                    </figure>



                    <figure class="gallery__item photo-360 alignCenter">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner" style="">
                        
                                    <div id="vrview" class="vrview"></div>

                            </div>
                        </div>
                    </figure>



                    <figure class="gallery__item inline-block">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg" />
                            </div>
                        </div>
                    </figure>



                    <figure class="gallery__item inline-block" style="width:68vw">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                


                                <video
                                    id="my-video"
                                    class="video-js"
                                    <?/*controls*/?>
                                    preload="auto"
                                    width="640"
                                    height="264"
                                    autoplay=true
                                    loop=true
                                    muted=true

                                    data-setup="{}"
                                  >
                                    <source src="<?php echo get_template_directory_uri(); ?>/video/2020-10-03-14-23-19__Whipple_Alpine_Meadow.mp4" type="video/mp4" />
                                    <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
                                    <p class="vjs-no-js">
                                      To view this video please enable JavaScript, and consider upgrading to a
                                      web browser that
                                      <a href="https://videojs.com/html5-video-support/" target="_blank"
                                        >supports HTML5 video</a
                                      >
                                    </p>
                                  </video>

                                  <!-- <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script> -->


                            </div>
                        </div>
                    </figure>





                    <figure class="gallery__item inline-block">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-12-48-56-Hillside-firedamage.jpg" />
                            </div>
                        </div>
                    </figure>



                    <figure class="gallery__item cropInsert alignCenter">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)"></div></div>
                    </figure>


                    <figure class="gallery__item textInsert">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, velit quis dapibus volutpat, odio sapien pulvinar arcu, nec pretium turpis urna ac metus. Phasellus interdum neque facilisis arcu porttitor, finibus euismod dolor consequat. Fusce ultrices ante eu leo commodo, et dignissim nibh ornare. Morbi tincidunt condimentum justo, vitae pharetra neque consectetur nec. Aenean ante nisi, suscipit sed venenatis vel, sollicitudin sit amet felis. Duis a aliquet nisi. Suspendisse cursus blandit mi. Cras et tortor in nisl egestas sollicitudin. Fusce tristique accumsan odio. Nullam vehicula hendrerit posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non pretium turpis. Etiam facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Integer posuere vel nibh quis rutrum. Nunc rhoncus metus interdum leo mollis, non pharetra ex feugiat. Vestibulum non imperdiet tortor. Sed suscipit vehicula nisi non scelerisque. Nulla facilisi. Nunc erat turpis, vulputate id volutpat non, lacinia congue nunc. Sed tempus massa sem, at sollicitudin neque mattis et. Nam ac odio aliquet metus ullamcorper luctus sed at lorem. Phasellus feugiat vitae orci vel ultrices. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. Integer sed nibh non quam elementum iaculis. Proin sagittis pharetra leo, in tincidunt eros. Sed faucibus, velit vitae mollis semper, nisl turpis aliquam orci, a venenatis sem mi ac lacus. Mauris vulputate dapibus dui pharetra accumsan. Sed scelerisque sodales placerat. Aenean non odio sed urna scelerisque ultricies.</p>

                                <p>Duis eu neque efficitur, finibus diam sed, finibus sapien. Suspendisse semper pulvinar neque eu iaculis. Proin justo tortor, ornare at augue at, dictum pulvinar odio. Nam volutpat lorem dui, id hendrerit lorem fringilla eget. Donec dignissim sagittis lorem, vel sagittis nisl. Nulla luctus massa ac lectus egestas ornare. Sed ut elit nibh.</p>

                                <p>Proin quis quam scelerisque, mattis purus ac, dignissim ipsum. Vestibulum sollicitudin eu dui a bibendum. Maecenas sollicitudin ullamcorper commodo. Mauris porta, nulla nec luctus ullamcorper, tortor nisi elementum tellus, et placerat lorem risus eget arcu. Donec sit amet tellus eu dolor maximus elementum vitae eget diam. Donec pellentesque nisl arcu, at condimentum purus lacinia at. Praesent quis diam libero. Mauris consequat lacinia ipsum vel rutrum. Proin consectetur ligula vitae purus iaculis, et mollis lacus sagittis.</p>
                            </div>
                        </div>
                    </figure>


                    <figure class="gallery__item cropInsert alignCenter">
                        <div class="gallery__item-img"><div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)"></div></div>
                    </figure>


                    <div class="gallery__text"><span class="gallery__text-inner" data-scroll data-scroll-speed="4">Chad</span><span data-scroll data-scroll-speed="1" class="gallery__text-inner">Chiliad</span></div>



                </div>
            </div>
        </main>





</body>

<!-- <script nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
<script nomodule src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CElement.prototype.append%2CNodeList.prototype.forEach%2CCustomEvent%2Csmoothscroll" crossorigin="anonymous"></script> -->

<!-- <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script> -->


<script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/locomotive-scroll.min.js"></script>

<script type="text/javascript">
(function(){

    // Initialize Locomotive Scroll (horizontal direction)
    const lscroll = new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true,
        direction: 'horizontal'
    });

})();
</script>


<script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script>

<script type="text/javascript">

    function onLoad() {
      vrView = new VRView.Player('#vrview', {
        width: '100%',
        height: 480,
        // image: '<?php echo get_template_directory_uri(); ?>/images/360/IMG_20201216_144237_00_005_test_.jpg',
        image: 'https://livingrecord.abbeyeverson.com/wp-content/themes/lrae_v0/images/360/IMG_20201216_144237_00_005_test_.jpg',
        is_stereo: false,
        is_autopan_off: true
      });

      // vrView.on('ready', onVRViewReady);
      // vrView.on('modechange', onModeChange);
      // vrView.on('getposition', onGetPosition);
      // vrView.on('error', onVRViewError);
    }
    
    window.addEventListener('load', onLoad);

</script>




    <?php 

        include get_template_directory() . '/js/bottomScripts.php';

    ?>






</html>