<?php
/**
 * Template Name: LighterPack
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

?>
<!doctype html>
<html class="" lang="en">
    <head>

    <?php get_header(); ?>

    <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />

<style>


</style>






</head>
<body>


    <?php 

        include get_template_directory() . '/nav.php';

    ?>


        <main data-scroll-container>
            <div class="content">
                <div class="gallery">
                    
                    <figure class="gallery__item">
                        <div class="gallery__item-img">
                            <div class="gallery__item-imginner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)">
                                

                                <iframe src="https://lighterpack.com/r/wpzwu6" height="500px" width="500px"></iframe>


                            </div>
                        </div>
                    </figure>




                </div>
            </div>
        </main>



<?php //get_footer(); ?>

</body>

<!-- <script nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
<script nomodule src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CElement.prototype.append%2CNodeList.prototype.forEach%2CCustomEvent%2Csmoothscroll" crossorigin="anonymous"></script> -->


<script src="<?php echo get_template_directory_uri(); ?>/js/locomotive-scroll.min.js"></script>


    <?php 

        include get_template_directory() . '/js/bottomScripts.php';

    ?>


<script type="text/javascript">
(function(){

    // Initialize Locomotive Scroll (horizontal direction)
    const lscroll = new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true,
        direction: 'horizontal'
    });

})();
</script>

</html>