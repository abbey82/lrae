<?php
/**
 * Template Name: Demo3
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

?>
<!doctype html>
<html class="" lang="en">
    <head>

    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.min.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css"> -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lraeStyle.css">
    <link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />
    <link href="<?php echo get_template_directory_uri(); ?>/css/photo-sphere-viewer.css" rel="stylesheet" />
    <!-- <link href="<?php echo get_template_directory_uri(); ?>/pannellum_example/pannellum.css" rel="stylesheet" /> -->

<style>


</style>






</head>
<body>

    <?php 

        include get_template_directory() . '/nav.php';

    ?>

    <div id="LightBoxOverlay">
        <!-- <img id="overlayImgSrc" src="" /> -->

        <div class="ZNPimage-container">
            <div id="divZoom" class="ZNPimage" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-12-48-56-Hillside-firedamage.jpg);"></div>
        </div>

    </div>


    <div id="VRBoxOverlay">

        <div id="vrview" class="vrview"></div>

    </div>










    <main data-scroll-container>
        <div class="content">
            <div class="gallery">
                
                
                
                
                <?php /*
                <figure class="cellItem inline-block" style="width:100vw">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner">
                            


                            <video
                                id="my-video-2"
                                class="video-js"
                                <?/*controls* /?>
                                preload="auto"
                                width="640"
                                height="264"
                                autoplay=true
                                loop=true
                                muted=true

                                data-setup="{}"
                              >
                                <source src="<?php echo get_template_directory_uri(); ?>/video/2021-01-09_test1.mp4" type="video/mp4" />
                                <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
                                <p class="vjs-no-js">
                                  To view this video please enable JavaScript, and consider upgrading to a
                                  web browser that
                                  <a href="https://videojs.com/html5-video-support/" target="_blank"
                                    >supports HTML5 video</a
                                  >
                                </p>
                              </video>



                        </div>
                    </div>
                </figure>
                */?>
                
                
                
                
                <!-- — — — — — — — — — 01 — — — — — — — — — — — — — — —  -->
                <figure class="cellItem FullFramedWindow grey0e">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner PhotoOverlayTrigger" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2021-05-26-19.48.33-Pano_ViewWhitney_fromArchRock-1__LARGE_v1.jpg); background-position: 50% 0px;">
                            <div class="PhotoDataOverlay">
                                <div class="BtmLeftData">
                                    <h2>Mt San Antonio (Baldy)</h2>
                                    <span>Nikon D300 &bull; Tamron 20-270mm</span>
                                    <span>1/100 sec &bull; f/1.8 &bull; focal 75mm</span>
                                </div>
                                <div class="BtmRightTrigger">
                                    &dash;more&dash;
                                </div>
                                <div class="masterTrigger">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </figure>



                <figure class="cellItem textInsert bg0E">
                    <!-- <div class="usedToBe_item_img"> -->
                        <div class="usedToBe_item_inner">

                            <div class="VAlignMiddleContainer">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, facilisis, lacus at efficitur  Nulla scelerisque, facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, bibendum consequat a a est. Orci varius natoque penatibus etbibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placeratturpis fermentum consequat a at sem. Nullam placerat dui turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. </p>
                            </div>
                            <!-- :after valign:middle -->

                        </div>
                    <!-- </div> -->
                </figure>



                <figure class="cellItem inline-block spacer w60">
                    <!-- SPACER – 60px -->
                </figure>



                <?/*
                <figure class="cellItem x150">
                    <div class="usedToBe_item_img"><div class="usedToBe_item_inner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)"></div></div>
                </figure>
                */?>



                <figure id="pano_001"   class="cellItem inline-block pano framed">

                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner PhotoOverlayTrigger">

                            <div class="PhotoDataOverlay">
                                <div class="BtmLeftData">
                                    <h2>Mt San Antonio (Baldy)</h2>
                                    <span>Nikon D300 &bull; Tamron 20-270mm</span>
                                    <span>1/100 sec &bull; f/1.8 &bull; focal 75mm</span>
                                </div>
                                <div class="BtmRightTrigger">
                                    &dash;more&dash;
                                </div>
                            </div>

                            <img src="<?php echo get_template_directory_uri(); ?>/images/IMG_4785_testV1.jpg" />
                        </div>
                    </div>
                    <div class="StickyPanoNotice" data-scroll data-scroll-sticky data-scroll-target="#pano_001">
                        <p>Mt Whitney - Looking West, Sunset</p>
                    </div>
                </figure>









                <figure class="cellItem textInsert bg0E">
                    <!-- <div class="usedToBe_item_img"> -->
                        <div class="usedToBe_item_inner">

                            <div class="VAlignMiddleContainer">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, facilisis, lacus at efficitur  Nulla scelerisque, facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, bibendum consequat a a est. Orci varius natoque penatibus etbibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placeratturpis fermentum consequat a at sem. Nullam placerat dui turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. </p>
                            </div>
                            <!-- :after valign:middle -->

                        </div>
                    <!-- </div> -->
                </figure>







                <!-- <figure class="cellItem inline-block" style="width:calc(100vh / 0.75)" data-width="1440" data-height="1080" <?/* data-scroll data-scroll-repeat  - this allows for class inView to be added */?>> -->
                <figure class="cellItem inline-block" style="width:calc(100vh / 0.5625)" data-width="1280" data-height="720" <?/* data-scroll data-scroll-repeat  - this allows for class inView to be added */?>>
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner">
                            


                            <video
                                id="my-video"
                                class="video-js"
                                <?/*controls*/?>
                                preload="auto"
                                width="1280"
                                height="720"
                                autoplay=true
                                loop=true
                                muted=true

                                data-setup="{}"
                              >
                                <source src="<?php echo get_template_directory_uri(); ?>/video/IMG_4788__720.mp4" type="video/mp4" />
                                <!-- <source src="MY_VIDEO.webm" type="video/webm" /> -->
                                <p class="vjs-no-js">
                                  To view this video please enable JavaScript, and consider upgrading to a
                                  web browser that
                                  <a href="https://videojs.com/html5-video-support/" target="_blank"
                                    >supports HTML5 video</a
                                  >
                                </p>
                              </video>

                              <!-- <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script> -->


                        </div>
                    </div>
                </figure>







                <figure class="cellItem textInsert bg0E">
                    <!-- <div class="usedToBe_item_img"> -->
                        <div class="usedToBe_item_inner">

                            <div class="VAlignMiddleContainer">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, facilisis, lacus at efficitur  Nulla scelerisque, facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, bibendum consequat a a est. Orci varius natoque penatibus etbibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placeratturpis fermentum consequat a at sem. Nullam placerat dui turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. </p>
                            </div>
                            <!-- :after valign:middle -->

                        </div>
                    <!-- </div> -->
                </figure>







                <figure class="cellItem inline-block pano framed">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner PhotoOverlayTrigger">

                            <div class="PhotoDataOverlay">
                                <div class="BtmLeftData">
                                    <h2>Mt San Antonio (Baldy)</h2>
                                    <span>Nikon D300 &bull; Tamron 20-270mm</span>
                                    <span>1/100 sec &bull; f/1.8 &bull; focal 75mm</span>
                                </div>
                                <div class="BtmRightTrigger">
                                    &dash;more&dash;
                                </div>
                            </div>

                            <img src="<?php echo get_template_directory_uri(); ?>/images/IMG_4875_testV1.jpg" />
                        </div>
                    </div>
                </figure>






                <figure class="cellItem inline-block spacer w5040 grad0E-F1">
                    <!-- SPACER – 60px -->

                    <div class="stickyContainer" id="stick"  >
                      <p data-scroll data-scroll-sticky data-scroll-target="#stick">
                        Mount Whitney<br><em>14,505'</em>
                      </p>
                    </div>

                </figure>





                <figure class="cellItemx thumbnail-grid bgF1" <?/*data-scroll data-scroll-repeat*/?>>
                    <div class="usedToBe_item_img-grid" style="">
                    <div class="usedToBe_item_img-grid-inner" style="width:100vh;height:100vh;">

                        <div class="gallery__img-grid-item lBox th1 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg"></div>
                        <div class="gallery__img-grid-item lBox th2 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg"></div>
                        <div class="gallery__img-grid-item lBox th3 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg"></div>
                        <div class="gallery__img-grid-item lBox th4 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg"></div>
                        <div class="gallery__img-grid-item lBox th5 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-10-04-44-48-Pano_of_high_desert.jpg"></div>
                        <div class="gallery__img-grid-item lBox th6 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th7 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th8 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th9 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th10" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th11" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th12" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th13" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th14" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th15" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item vBox th16" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/360/IMG_20201216_144237_00_005_test_.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>

                    </div>
                    </div>
                </figure>



                <figure class="cellItemx thumbnail-grid bgF1" <?/*data-scroll data-scroll-repeat*/?>>
                    <div class="usedToBe_item_img-grid" style="">
                    <div class="usedToBe_item_img-grid-inner" style="width:100vh;height:100vh;">

                        <div class="bTop"></div>
                        <div class="gallery__img-grid-item lBox th1 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg"></div>
                        <div class="gallery__img-grid-item lBox th2 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg"></div>
                        <div class="gallery__img-grid-item lBox th3 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg"></div>
                        <div class="gallery__img-grid-item lBox th4 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-08-07-20-02-18__viewofBaldy_twilight.jpg"></div>
                        <div class="gallery__img-grid-item lBox th5 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th6 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th7 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th8 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th9 " style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th10" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th11" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th12" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg"></div>
                        <div class="gallery__img-grid-item lBox th13" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg"></div>
                        <div class="gallery__img-grid-item lBox th14" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg"></div>
                        <div class="gallery__img-grid-item lBox th15" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg"></div>
                        <div class="gallery__img-grid-item lBox th16" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg)" data-src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg"></div>
                        <div class="bBtm"></div>

                    </div>
                    </div>
                </figure>


                <figure class="cellItem inline-block spacer w120 bgF1">
                    <!-- SPACER – 60px -->
                </figure>

                <figure class="cellItem inline-block">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg" />
                        </div>
                    </div>
                </figure>



                <?php /*
                <figure class="cellItem photo-360 alignCenter">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner" style="">
                    
                                <div id="vrview" class="vrview"></div>

                        </div>
                    </div>
                </figure>
                */ ?>



                <figure id="hey" class="cellItem inline-block pano framed">
                    <div class="" data-scroll-id="#testTargetScrollTo" data-scroll-target="#testTargetScrollTo">
                       test...
                    </div>
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner PhotoOverlayTrigger">

                            <div class="PhotoDataOverlay">
                                <div class="BtmLeftData">
                                    <h2>Mt San Antonio (Baldy)</h2>
                                    <span>Nikon D300 &bull; Tamron 20-270mm</span>
                                    <span>1/100 sec &bull; f/1.8 &bull; focal 75mm</span>                              
                                </div>
                                <div class="BtmRightTrigger">
                                    &dash;more&dash;
                                </div>
                            </div>

                            <img src="<?php echo get_template_directory_uri(); ?>/images/2020-12-04-16-42-37-Pano__HUGE_VIEW_OF_TIMBER_BALDY.jpg" />
                        </div>
                    </div>
                    <div class="StickyPanoNotice" data-scroll data-scroll-sticky data-scroll-target="#hey">
                        <p>Angeles National Forest</p>
                    </div>
                </figure>



                <?/*
                <figure class="cellItem inline-block" style="width:60px">
                    <!-- SPACER – 60px -->
                </figure>
                */?>



                <figure class="cellItem textInsert bg0E">
                    <!-- <div class="usedToBe_item_img"> -->
                        <div class="usedToBe_item_inner">

                            <div class="VAlignMiddleContainer"
                                    <?/*
                                    role="img"
                                    aria-label=""
                                    data-scroll
                                    data-testTargetScrollTo="vertical"
                                    data-scroll-speed="3"
                                    */?>

                                >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, facilisis, lacus at efficitur  Nulla scelerisque, facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, bibendum consequat a a est. Orci varius natoque penatibus etbibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placeratturpis fermentum consequat a at sem. Nullam placerat dui turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. </p>
                            </div>
                            <!-- :after valign:middle -->

                        </div>
                    <!-- </div> -->
                </figure>


                <figure class="cellItem textInsert bg0E">
                    <!-- <div class="usedToBe_item_img"> -->
                        <div class="usedToBe_item_inner">

                            <div class="VAlignMiddleContainer"
                                    <?/*
                                    role="img"
                                    aria-label=""
                                    data-scroll
                                    data-testTargetScrollTo="vertical"
                                    data-scroll-speed="3"
                                    */?>

                                >
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat consequat metus id gravida. Nulla scelerisque, facilisis, lacus at efficitur  Nulla scelerisque, facilisis, lacus at efficitur dictum, nisl velit feugiat augue, eget blandit lectus ante in tortor.</p>

                                <p>Praesent efficitur sem sed ante bibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, bibendum consequat a a est. Orci varius natoque penatibus etbibendum consequat a a est. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus ac lorem in mollis.</p>

                                <p>Morbi non nisl eget turpis fermentum consequat a at sem. Nullam placeratturpis fermentum consequat a at sem. Nullam placerat dui turpis fermentum consequat a at sem. Nullam placerat dui ornare auctor dictum. </p>
                            </div>
                            <!-- :after valign:middle -->

                        </div>
                    <!-- </div> -->
                </figure>



                <figure class="cellItem inline-block">
                    <div class="usedToBe_item_img">
                        <div class="usedToBe_item_inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/2020-08-08-12-48-56-Hillside-firedamage.jpg" />
                        </div>
                    </div>
                </figure>






                <figure class="cellItem cropInsert alignCenter">
                    <div class="usedToBe_item_img"><div class="usedToBe_item_inner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)"></div></div>
                </figure>



                <figure class="cellItem cropInsert alignCenter">
                    <div class="usedToBe_item_img"><div class="usedToBe_item_inner" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/2020-07-13-11-13-56__LIZARD.jpg)"></div></div>
                </figure>


                <div class="gallery__text"><span class="gallery__text-inner" data-scroll data-scroll-speed="4">abbey</span><span data-scroll data-scroll-speed="1" class="gallery__text-inner">Chiliad</span></div>



            </div>
        </div>
    </main>





</body>

<!-- <script nomodule src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js" crossorigin="anonymous"></script>
<script nomodule src="https://polyfill.io/v3/polyfill.min.js?features=Object.assign%2CElement.prototype.append%2CNodeList.prototype.forEach%2CCustomEvent%2Csmoothscroll" crossorigin="anonymous"></script> -->

<script src="<?php echo get_template_directory_uri(); ?>/js/three.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/browser.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/photo-sphere-viewer.js"></script>


<!-- <script src="<?php echo get_template_directory_uri(); ?>/pannellum_example/pannellum.js"></script> -->


<!-- <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script> -->


<!-- <script src="https://vjs.zencdn.net/7.10.2/video.min.js"></script> -->

<script src="<?php echo get_template_directory_uri(); ?>/js/locomotive-scroll.min.js"></script>
<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/locomotive-scroll@3.5.4/dist/locomotive-scroll.css">  -->
<!-- <script src="https://cdn.jsdelivr.net/npm/locomotive-scroll@3.5.4/dist/locomotive-scroll.min.js"> -->

<script type="text/javascript">
(function(){

    // Initialize Locomotive Scroll (horizontal direction)
    const lscroll = new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true,
        direction: 'horizontal'
    });

})();
</script>


<!-- <script src="https://storage.googleapis.com/vrview/2.0/build/vrview.min.js"></script> -->

<script type="text/javascript">

    /*
    function onLoad() {
      vrView = new VRView.Player('#vrview', {
        width: '100%',
        height: 480,
        // image: '<?php echo get_template_directory_uri(); ?>/images/360/IMG_20201216_144237_00_005_test_.jpg',
        image: 'https://livingrecord.abbeyeverson.com/wp-content/themes/lrae_v0/images/360/IMG_20201216_144237_00_005_test_.jpg',
        is_stereo: false,
        is_autopan_off: true
      });

      // vrView.on('ready', onVRViewReady);
      // vrView.on('modechange', onModeChange);
      // vrView.on('getposition', onGetPosition);
      // vrView.on('error', onVRViewError);
    }
    
    window.addEventListener('load', onLoad);
    */












    let options = {
        root: null,
        rootMargin: '0px',
        threshold: 0.01
    };


    let callback = (entries, observer)=> {
        entries.forEach(entry => {
            if(entry.target.id == 'my-video') {

                // console.log('testing from within entries loop.');
                
                if(entry.isIntersecting) {
                    entry.target.play();

                } else {
                    entry.target.pause();

                }

            }

        })
    }

    let observer = new IntersectionObserver(callback, options);
    observer.observe(document.querySelector('#my-video'));







</script>




    <?php 

        include get_template_directory() . '/js/bottomScripts.php';

    ?>






</html>